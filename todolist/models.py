from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class ToDoItem(models.Model):
	task_name = models.CharField(max_length = 50)
	description = models.CharField(max_length = 200)
	status = models.CharField(max_length = 50, default = "pending")
	date_created = models.DateTimeField('date created')
	user = models.ForeignKey(User, on_delete=models.CASCADE, default="")


class User(models.Model):
	username = models.CharField(max_length = 50)
	first_name = models.CharField(max_length = 50)
	last_name = models.CharField(max_length = 50)
	email = models.CharField(max_length = 50)
	password = models.CharField(max_length = 50)
	is_staff = models.CharField(max_length = 50, default = False)
	is_active = models.CharField(max_length = 50, default = True)
	user = models.ForeignKey(User, on_delete=models.CASCADE, default="")


class EventItem(models.Model):
	event_name = models.CharField(max_length = 50)
	description = models.CharField(max_length = 200)
	status = models.CharField(max_length = 50, default = "pending")
	event_date = models.DateTimeField('event date')
	user = models.ForeignKey(User, on_delete=models.CASCADE, default="")
