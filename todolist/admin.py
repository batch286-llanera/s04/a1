from django.contrib import admin

# Register your models here.
from .models import ToDoItem
admin.site.register(ToDoItem)

from .models import EventItem
admin.site.register(EventItem)

from .models import User
admin.site.register(User)
